package br.com.builders.treinamento;

import br.com.builders.treinamento.builders.CustomerBuilder;
import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.request.CustomerRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class TestUtils {

    private static final String TEST_CUSTOMER_NAME = "Customer Teste";

    private static final String TEST_CUSTOMER_CRM_ID = "C12345";

    private static final String TEST_CUSTOMER_BASE_URL = "http://platform.builders/";

    private static final String TEST_CUSTOMER_LOGIN_PREFIX = "customer.teste";

    public static List<Customer> createCustomerList(Integer quantity){
        final AtomicInteger count = new AtomicInteger(1);
        List<Customer> customers = new ArrayList<>();
        while(count.get() <= quantity){
            Customer customer = new CustomerBuilder().with( $ -> {
                $.crmId = TEST_CUSTOMER_CRM_ID + count.get();
                $.baseUrl = TEST_CUSTOMER_BASE_URL + count;
                $.name = TEST_CUSTOMER_NAME + " " + count;
                $.login = TEST_CUSTOMER_LOGIN_PREFIX + count + "@junit.com";
            }).build();
            customers.add(customer);
            count.getAndIncrement();
        }
        return customers;
    }

    public static Customer createCustomer(){
        Customer customer = new CustomerBuilder().with( $ -> {
            $.crmId = TEST_CUSTOMER_CRM_ID;
            $.baseUrl = TEST_CUSTOMER_BASE_URL;
            $.name = TEST_CUSTOMER_NAME;
            $.login = TEST_CUSTOMER_LOGIN_PREFIX  + "@junit.com";
        }).build();
        return customer;
    }

    public static CustomerRequest createCustomerRequest(String id, String crmId, String baseUrl,
                                                                  String name, String login){
        CustomerRequest cr = new CustomerRequest(id, crmId, baseUrl, name, login);
        return cr;
    }

    public static Customer createCustomerFromRequest(CustomerRequest cr) {
        return new CustomerBuilder().with($ -> {
            $.id = cr.getId();
            $.crmId = cr.getCrmId();
            $.name = cr.getName();
            $.baseUrl = cr.getBaseUrl();
            $.login = cr.getLogin();
        }).build();
    }

    public static String asJsonString(final Object obj) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            final String jsonContent = mapper.writeValueAsString(obj);
            return jsonContent;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
