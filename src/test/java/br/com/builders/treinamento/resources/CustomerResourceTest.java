package br.com.builders.treinamento.resources;

import br.com.builders.treinamento.TestUtils;
import br.com.builders.treinamento.TreinamentoApplication;
import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.request.CustomerRequest;
import br.com.builders.treinamento.exception.BadRequestAPIException;
import br.com.builders.treinamento.exception.ErrorMessage;
import br.com.builders.treinamento.exception.NotFoundException;
import br.com.builders.treinamento.exception.UnprocessableEntityAPIException;
import br.com.builders.treinamento.repository.CustomerRepository;
import org.hamcrest.Matchers;
import org.hamcrest.collection.IsCollectionWithSize;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.fail;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TreinamentoApplication.class)
public class CustomerResourceTest {

    private MockMvc mockMvc;
    @Mock
    private CustomerRepository repository;
    @InjectMocks
    private CustomerResource customerResource;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(customerResource).build();
    }

    @Test
    public void getAllCustomers() throws Exception {
        List<Customer> customerList = TestUtils.createCustomerList(3);
        Mockito.when(repository.findAll()).thenReturn(customerList);
        RequestBuilder request = MockMvcRequestBuilders.get("/api/customers");
        mockMvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$", IsCollectionWithSize.hasSize(3)));
    }

    @Test
    public void getCustomer() throws Exception {
        Customer customer = TestUtils.createCustomer();
        Mockito.when(repository.findOne(customer.get_id().toHexString())).thenReturn(customer);
        RequestBuilder request = MockMvcRequestBuilders.get("/api/customers/" + customer.get_id().toHexString());
        mockMvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(customer.get_id().toHexString())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.crmId", Matchers.is(customer.getCrmId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.baseUrl", Matchers.is(customer.getBaseUrl().toString())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is(customer.getName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.login", Matchers.is(customer.getLogin())));
    }

    @Test
    public void getCustomerNotFound() {
        try {
            Customer customer = TestUtils.createCustomer();
            Mockito.when(repository.findOne(customer.get_id().toHexString())).thenReturn(null);

            RequestBuilder request = MockMvcRequestBuilders.get("/api/customers/{id}", customer.get_id().toHexString());
            mockMvc.perform(request);
            fail();
        } catch (Exception e) {
            Assert.assertTrue(e.getCause() instanceof NotFoundException);
            NotFoundException nf = (NotFoundException) e.getCause();
            Assert.assertEquals(nf.getStatus(), HttpStatus.NOT_FOUND);
            Assert.assertEquals(nf.getCause().getMessage(), "The customer is not found!");
        }
    }

    @Test
    public void putCustomerSuccessfully() throws Exception {
        Customer customer = TestUtils.createCustomer();

        CustomerRequest cr = TestUtils.createCustomerRequest("5b967d4b5fd69e0f830f0c04",
                "C974831", "http://www.platform-builders.com/updated",
                "Builders Test Updated", "platform.updated@builders.com");

        Mockito.when(repository.findOne(cr.getId())).thenReturn(customer);
        Mockito.when(repository.save(Mockito.any(Customer.class))).thenReturn(TestUtils.createCustomerFromRequest(cr));
        String json = TestUtils.asJsonString(cr);

        RequestBuilder request = MockMvcRequestBuilders.put("/api/customers/{id}", cr.getId())
                .contentType(MediaType.APPLICATION_JSON).content(json);
        mockMvc.perform(request).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(cr.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.is("Item replaced!")));

    }

    @Test
    public void putCustomerNotFoundFails() {
        try {
            CustomerRequest cr = TestUtils.createCustomerRequest("non_existent_id",
                    "C974831", "http://www.platform-builders.com/updated",
                    "Builders Test Updated", "platform.updated@builders.com");

            Mockito.when(repository.findOne(cr.getId())).thenReturn(null);
            String json = TestUtils.asJsonString(cr);

            RequestBuilder request = MockMvcRequestBuilders.put("/api/customers/{id}", cr.getId())
                    .contentType(MediaType.APPLICATION_JSON).content(json);
            mockMvc.perform(request);
            fail();
        } catch (Exception e) {
            Assert.assertTrue(e.getCause() instanceof NotFoundException);
            NotFoundException nf = (NotFoundException) e.getCause();
            Assert.assertEquals(nf.getStatus(), HttpStatus.NOT_FOUND);
            Assert.assertEquals(nf.getCause().getMessage(), "The customer is not found!");
        }
    }

    @Test
    public void putCustomerWithInvalidValues() {
        try {
            Customer customer = TestUtils.createCustomer();

            CustomerRequest cr = TestUtils.createCustomerRequest("5b967d4b5fd69e0f830f0c04",
                    "C645235", "malformedurlvalue", "Builders Test", "malformedemail");

            Mockito.when(repository.findOne(cr.getId())).thenReturn(customer);
            String json = TestUtils.asJsonString(cr);

            RequestBuilder request = MockMvcRequestBuilders.put("/api/customers/{id}", cr.getId())
                    .contentType(MediaType.APPLICATION_JSON).content(json);
            mockMvc.perform(request);
            fail();
        } catch (Exception e) {
            Assert.assertTrue(e.getCause() instanceof BadRequestAPIException);
            BadRequestAPIException br = (BadRequestAPIException) e.getCause();
            Assert.assertTrue(br.getErros().size() == 2);
            Assert.assertTrue(br.getErros().stream().map(ErrorMessage::getMessage).anyMatch(m -> m.equals("Invalid URL format.")));
            Assert.assertTrue(br.getErros().stream().map(ErrorMessage::getMessage).anyMatch(m -> m.equals("Invalid email format.")));
            Assert.assertTrue(br.getErros().stream().map(ErrorMessage::getCode).anyMatch(c -> c.equals("baseUrl")));
            Assert.assertTrue(br.getErros().stream().map(ErrorMessage::getCode).anyMatch(c -> c.equals("login")));
            Assert.assertTrue(br.getErros().stream().map(ErrorMessage::getLink).allMatch(l -> l.equals("customerRequest")));
        }
    }

    @Test
    public void patchCustomerSuccessfully() throws Exception {
        final String PATCH_URL = "http://updated.url.com/index.html";
        final String PATCH_USERNAME = "Patch Test";

        Customer customer = TestUtils.createCustomer();
        Customer customerPatched = TestUtils.createCustomer();

        customerPatched.setBaseUrl(new URL(PATCH_URL));
        customerPatched.setName(PATCH_USERNAME);

        Map<String, Object> patchObject = new HashMap<>();
        patchObject.put("baseUrl", PATCH_URL);
        patchObject.put("name", PATCH_USERNAME);
        JSONObject json = new JSONObject(patchObject);

        Mockito.when(repository.findOne(customer.get_id().toHexString())).thenReturn(customer);
        Mockito.when(repository.save(Mockito.any(Customer.class))).thenReturn(customerPatched);

        RequestBuilder request = MockMvcRequestBuilders.patch("/api/customers/{id}", customer.get_id().toHexString())
                .contentType(MediaType.APPLICATION_JSON).content(json.toString());

        mockMvc.perform(request).andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(customer.get_id().toHexString())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.is("Item modified!")));
    }

    @Test
    public void patchCustomerNotFoundFails(){
        try {
            final String PATCH_URL = "http://updated.url.com/index.html";
            final String PATCH_USERNAME = "Patch Test";

            Customer customer = TestUtils.createCustomer();
            Customer customerPatched = TestUtils.createCustomer();

            Mockito.when(repository.findOne(customer.get_id().toHexString())).thenReturn(null);

            customerPatched.setBaseUrl(new URL(PATCH_URL));
            customerPatched.setName(PATCH_USERNAME);

            Map<String, Object> patchObject = new HashMap<>();
            patchObject.put("baseUrl", PATCH_URL);
            patchObject.put("name", PATCH_USERNAME);
            JSONObject json = new JSONObject(patchObject);

            RequestBuilder request = MockMvcRequestBuilders.patch("/api/customers/{id}", customer.get_id().toHexString())
                    .contentType(MediaType.APPLICATION_JSON).content(json.toString());
            mockMvc.perform(request);
            fail();
        } catch (Exception e) {
            Assert.assertTrue(e.getCause() instanceof NotFoundException);
            NotFoundException nf = (NotFoundException) e.getCause();
            Assert.assertEquals(nf.getStatus(), HttpStatus.NOT_FOUND);
            Assert.assertEquals(nf.getCause().getMessage(), "The customer is not found!");
        }
    }

    @Test
    public void postCustomerSuccessfully() throws Exception {
        CustomerRequest cr = TestUtils.createCustomerRequest("5b967d4b5fd69e0f830f0c04",
                "C645235", "http://www.platform-builders.com",
                "Builders Test", "platform@builders.com");

        String json = TestUtils.asJsonString(cr);
        Mockito.when(repository.save(Mockito.any(Customer.class))).thenReturn(TestUtils.createCustomerFromRequest(cr));
        RequestBuilder request = MockMvcRequestBuilders.post("/api/customers")
                .contentType(MediaType.APPLICATION_JSON).content(json);
        mockMvc.perform(request).andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(cr.getId())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.is("Item created!")));
    }

    @Test
    public void postCustomerWithInvalidValues() {
        try {
            //Create customer request with invalid values
            CustomerRequest cr = TestUtils.createCustomerRequest("5b967d4b5fd69e0f830f0c04",
                    "C645235", "malformedurlvalue", "Builders Test", "malformedemail");
            String json = TestUtils.asJsonString(cr);
            RequestBuilder request = MockMvcRequestBuilders.post("/api/customers")
                    .contentType(MediaType.APPLICATION_JSON).content(json);
            mockMvc.perform(request);
            fail();
        } catch (Exception e) {
            Assert.assertTrue(e.getCause() instanceof BadRequestAPIException);
            BadRequestAPIException br = (BadRequestAPIException) e.getCause();
            Assert.assertTrue(br.getErros().size() == 2);
            Assert.assertTrue(br.getErros().stream().map(ErrorMessage::getMessage).anyMatch(m -> m.equals("Invalid URL format.")));
            Assert.assertTrue(br.getErros().stream().map(ErrorMessage::getMessage).anyMatch(m -> m.equals("Invalid email format.")));
            Assert.assertTrue(br.getErros().stream().map(ErrorMessage::getCode).anyMatch(c -> c.equals("baseUrl")));
            Assert.assertTrue(br.getErros().stream().map(ErrorMessage::getCode).anyMatch(c -> c.equals("login")));
            Assert.assertTrue(br.getErros().stream().map(ErrorMessage::getLink).allMatch(l -> l.equals("customerRequest")));
        }
    }

    @Test
    public void postCustomerAlreadyExistsFails() {
        try {
            CustomerRequest cr = TestUtils.createCustomerRequest("5b967d4b5fd69e0f830f0c04",
                    "C645235", "http://www.platform-builders.com",
                    "Builders Test", "platform@builders.com");

            Customer c = TestUtils.createCustomerFromRequest(cr);

            Mockito.when(repository.findByLogin(cr.getLogin())).thenReturn(c);
            String json = TestUtils.asJsonString(cr);
            RequestBuilder request = MockMvcRequestBuilders.post("/api/customers")
                    .contentType(MediaType.APPLICATION_JSON).content(json);
            mockMvc.perform(request);
            fail();
        } catch (Exception e) {
            Assert.assertTrue(e.getCause() instanceof UnprocessableEntityAPIException);
            UnprocessableEntityAPIException ue = (UnprocessableEntityAPIException) e.getCause();

            Assert.assertTrue(ue.getErros().size() > 0);
            Assert.assertEquals(ue.getStatus(), HttpStatus.UNPROCESSABLE_ENTITY);
            Assert.assertTrue(ue.getErros().stream().map(ErrorMessage::getMessage).anyMatch(m -> m.equals("The item (platform@builders.com) already exists.")));
        }
    }

    @Test
    public void deleteCustomerSuccessfully() throws Exception {
        Customer customer = TestUtils.createCustomer();
        Mockito.when(repository.findOne(customer.get_id().toHexString())).thenReturn(customer);
        Mockito.doNothing().when(repository).delete(customer.get_id().toHexString());

        RequestBuilder request = MockMvcRequestBuilders.delete("/api/customers/{id}", customer.get_id().toHexString());
        mockMvc.perform(request)
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is(customer.get_id().toHexString())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message", Matchers.is("Item deleted!")));
    }

    @Test
    public void deleteCustomerNotFoundFails() {
        try {
            Customer customer = TestUtils.createCustomer();
            Mockito.when(repository.findOne(customer.get_id().toHexString())).thenReturn(customer);
            Mockito.doNothing().when(repository).delete(customer.get_id().toHexString());

            RequestBuilder request = MockMvcRequestBuilders.delete("/api/customers/{id}", "invalid_id_value");
            mockMvc.perform(request);
            fail();
        } catch (Exception e) {
            Assert.assertTrue(e.getCause() instanceof NotFoundException);
            NotFoundException nf = (NotFoundException) e.getCause();
            Assert.assertEquals(nf.getStatus(), HttpStatus.NOT_FOUND);
            Assert.assertEquals(nf.getCause().getMessage(), "The customer is not found!");
        }
    }


}
