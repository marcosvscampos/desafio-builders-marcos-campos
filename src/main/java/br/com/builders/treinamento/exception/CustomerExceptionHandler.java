package br.com.builders.treinamento.exception;

import br.com.builders.treinamento.resources.CustomerResource;
import br.com.builders.treinamento.utils.APIResponseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice (assignableTypes = CustomerResource.class)
public class CustomerExceptionHandler extends ResponseEntityExceptionHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(CustomerExceptionHandler.class);

    @ExceptionHandler(NotFoundException.class)
    protected ResponseEntity<Object> handleNotFoundException(NotFoundException e){
        e.printStackTrace();
        logE(e);
        return APIResponseUtils.generateStatusResponse(e.getId(), e.getCause().getMessage(), 404);
    }

    @ExceptionHandler(BadRequestAPIException.class)
    protected ResponseEntity<Object> handleBadRequestException(BadRequestAPIException e){
        e.printStackTrace();
        logE(e);
        return APIResponseUtils.generateBadRequestStatus(e.getErros());
    }

    @ExceptionHandler(UnprocessableEntityAPIException.class)
    protected ResponseEntity<Object> handleUnprocessableEntityException(UnprocessableEntityAPIException e){
        e.printStackTrace();
        logE(e);
        return APIResponseUtils.generateStatusResponse(e.getId(), e.getErros().get(0).getMessage(), 409);
    }

    private static void logE(final Exception e) {
        LOGGER.info("e={},m={}", e.getClass().getSimpleName(), e.getMessage());
    }


}
