package br.com.builders.treinamento.repository;

import br.com.builders.treinamento.domain.Customer;
import org.bson.types.ObjectId;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.mongodb.repository.MongoRepository;

@Lazy
public interface CustomerRepository extends MongoRepository<Customer, String> {

    public Customer findBy_id(ObjectId _id);

    public Customer findByLogin(String login);

}
