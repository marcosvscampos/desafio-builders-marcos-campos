package br.com.builders.treinamento.resources;

import br.com.builders.treinamento.builders.CustomerBuilder;
import br.com.builders.treinamento.builders.CustomerResponseBuilder;
import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.request.CustomerRequest;
import br.com.builders.treinamento.dto.response.CustomerResponse;
import br.com.builders.treinamento.exception.BadRequestAPIException;
import br.com.builders.treinamento.exception.NotFoundException;
import br.com.builders.treinamento.exception.UnprocessableEntityAPIException;
import br.com.builders.treinamento.repository.CustomerRepository;
import br.com.builders.treinamento.utils.APIResponseUtils;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestController
@RequestMapping(value = "/api/customers")
public class CustomerResource {

    @Autowired
    private CustomerRepository repository;

    private static Logger LOGGER = LoggerFactory.getLogger(CustomerResource.class);

    @RequestMapping(method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity postCustomer(@RequestBody @Valid CustomerRequest cr, Errors errors)
            throws BadRequestAPIException, UnprocessableEntityAPIException {

        LOGGER.info("Starting POST of Customer -> " + cr.getLogin() + " | Checking its existence in database...");
        Customer c = repository.findByLogin(cr.getLogin());

        if(Objects.nonNull(c)){
            LOGGER.error("There was an error during Customer creation -> The item (" + cr.getLogin() + ") already exists.");
            throw new UnprocessableEntityAPIException("The item (" + cr.getLogin() + ") already exists.", c.get_id().toHexString());
        }

        LOGGER.info("The Customer creation is available for the login " + cr.getLogin() + ", proceeding...");
        LOGGER.info("Checking object integrity...");
        if (errors.hasErrors()) {
            LOGGER.error("There was an error during Customer creation -> The Customer object has invalid values: " + errors.getFieldErrors());
            throw new BadRequestAPIException(APIResponseUtils.formatErrorsList(errors));
        }
        LOGGER.info("Object has been passed with valid values, proceeding to save process...");
        Customer savedC =repository.save(buildPersistenceObject(cr));
        LOGGER.info("Save process occurred successfully! -> " + savedC.toString());

        return APIResponseUtils.generateStatusResponse(savedC.get_id().toHexString(), "Item created!", 201);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity putCustomer(@PathVariable String id, @RequestBody @Valid CustomerRequest cr, Errors errors)
            throws BadRequestAPIException, NotFoundException {
        LOGGER.info("Starting PUT of Customer -> (ID: " + cr.getId() + ", Login: " + cr.getLogin() + ") | Checking its existence in database...");
        Customer customer = repository.findOne(id);

        if(Objects.isNull(customer)){
            LOGGER.error("There was an error during Customer update -> The item (" + cr.getId() + ") doesn't exist");
            throw new NotFoundException("The customer is not found!", id);
        }

        LOGGER.info("The Customer update is available for the ID " + cr.getId() + ", proceeding...");
        LOGGER.info("Checking object integrity...");
        if(errors.hasErrors()){
            LOGGER.error("There was an error during Customer update -> The Customer object has invalid values: " + errors.getFieldErrors());
            throw new BadRequestAPIException(APIResponseUtils.formatErrorsList(errors));
        }

        LOGGER.info("Object has been passed with valid values, proceeding to update process...");
        Customer updatedC = repository.save(buildPersistenceObject(cr));
        LOGGER.info("Update process occurred successfully! -> " + updatedC.toString());

        return APIResponseUtils.generateStatusResponse(updatedC.get_id().toHexString(), "Item replaced!", 200);
    }

    @RequestMapping(method = RequestMethod.PATCH, value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity patchCustomer(@PathVariable String id,
                                        @RequestBody Map<String, Object> partialCustomer) throws NotFoundException {
        LOGGER.info("Starting PATCH of Customer -> (ID: " + id + ") | Checking its existence in database...");
        Customer customer = repository.findOne(id);
        if(Objects.isNull(customer)){
            LOGGER.error("There was an error during Customer patch -> The item (" + id + ") doesn't exist");
            throw new NotFoundException("The customer is not found!", id);
        }

        LOGGER.info("The Customer patch is available for the ID " + id + ", proceeding...");
        partialCustomer.forEach((k, v) -> {
            try {
                LOGGER.info("Patching value -> " + v + " from attribute -> " + k);
                if(k.equalsIgnoreCase("baseUrl")){
                    URL baseUrl = new URL((String) v);
                    BeanUtils.setProperty(customer, k, baseUrl);
                } else {
                    BeanUtils.setProperty(customer, k, v);
                }
            } catch (Exception e){
                LOGGER.error("There was an error during Customer patch: " + e.getMessage());
                e.printStackTrace();
            }
        });

        LOGGER.info("Object values has been patched successfully, proceeding to update process...");
        Customer patchedC = repository.save(customer);
        LOGGER.info("Update process occurred successfully! -> " + patchedC.toString());

        return APIResponseUtils.generateStatusResponse(id, "Item modified!", 200);
    }


    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity deleteCustomer(@PathVariable String id) throws NotFoundException {
        LOGGER.info("Starting DELETE of Customer -> (ID: " + id + ") | Checking its existence in database...");
        Customer customer = repository.findOne(id);

        if(Objects.isNull(customer)){
            LOGGER.error("There was an error during Customer delete -> The item (" + id + ") doesn't exist");
            throw new NotFoundException("The customer is not found!", id);
        }

        LOGGER.info("The Customer delete is available for the ID " + id + ", proceeding...");
        repository.delete(customer);
        LOGGER.info("Delete process occurred successfully, object erased! ID -> " + id);

        return APIResponseUtils.generateStatusResponse(id, "Item deleted!", 200);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getCustomer(@PathVariable String id) throws NotFoundException {
        LOGGER.info("Starting GET of Customer -> (ID: " + id + ") | Checking its existence in database...");
        Customer ct = repository.findOne(id);

        if(Objects.isNull(ct)){
            LOGGER.error("There was an error during Customer get -> The item (" + id + ") doesn't exist");
            throw new NotFoundException("The customer is not found!", id);
        }

        LOGGER.info("The Customer is available for the ID " + id + ", proceeding...");
        CustomerResponse cr = buildResponseObject(ct);
        LOGGER.info("GET process occurred successfully, returning object! -> " + cr.toString());

        return APIResponseUtils.generateSuccessStatus(cr);
    }

    @RequestMapping(method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CustomerResponse>> getAllCustomers(){
        LOGGER.info("Starting GET of all Customers available in database...");
        List<Customer> customers = repository.findAll();

        List<CustomerResponse> customersResponse = new ArrayList<>();

        customers.forEach( ct -> {
            customersResponse.add(buildResponseObject(ct));
        });
        LOGGER.info("GET process occurred successfully, returning objects! -> " + customersResponse);
        return APIResponseUtils.generateSuccessStatus(customersResponse);
    }

    private CustomerResponse buildResponseObject(Customer ct){
        return new CustomerResponseBuilder().with($ -> {
            $.id = ct.get_id().toHexString();
            $.crmId = ct.getCrmId();
            $.baseUrl = ct.getBaseUrl().toString();
            $.login = ct.getLogin();
            $.name = ct.getName();
        }).build();
    }

    private Customer buildPersistenceObject(CustomerRequest cr){
       return new CustomerBuilder().with( $ -> {
            $.id = cr.getId();
            $.crmId = cr.getCrmId();
            $.name = cr.getName();
            $.baseUrl = cr.getBaseUrl();
            $.login = cr.getLogin();
       }).build();
    }
}
