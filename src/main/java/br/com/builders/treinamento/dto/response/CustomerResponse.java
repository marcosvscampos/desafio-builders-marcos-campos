package br.com.builders.treinamento.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Data
@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class CustomerResponse {

    private String id;
    private String crmId;
    private String baseUrl;
    private String name;
    private String login;
    private String message;

    public CustomerResponse(String id, String crmId, String baseUrl, String name, String login) {
        this.id = id;
        this.crmId = crmId;
        this.baseUrl = baseUrl;
        this.name = name;
        this.login = login;
    }

    public CustomerResponse(String id, String message){
        this.id = id;
        this.message = message;
    }
}
