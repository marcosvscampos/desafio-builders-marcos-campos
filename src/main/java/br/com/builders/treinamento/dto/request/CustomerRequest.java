package br.com.builders.treinamento.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CustomerRequest implements Serializable {

    private String id;
    @NotNull (message = "The crmId value cannot be null.")
    private String crmId;
    @NotNull (message = "The baseUrl value cannot be null.")
    @Pattern(regexp = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]",
            message = "Invalid URL format.")
    private String baseUrl;
    @NotNull (message = "The name value cannot be null.")
    @Size(min = 5, max = 50, message = "The name size must have between 5 and 50 characters.")
    private String name;
    @NotNull (message = "The email value cannot be null.")
    @Email (message = "Invalid email format.")
    private String login;

}
