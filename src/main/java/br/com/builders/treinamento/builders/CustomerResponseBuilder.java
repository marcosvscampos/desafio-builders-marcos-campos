package br.com.builders.treinamento.builders;


import br.com.builders.treinamento.dto.response.CustomerResponse;

import java.util.function.Consumer;

public class CustomerResponseBuilder {

    public String id;
    public String crmId;
    public String baseUrl;
    public String name;
    public String login;
    public String message;

    public CustomerResponseBuilder with(
            Consumer<CustomerResponseBuilder> builderFunction) {
        builderFunction.accept(this);
        return this;
    }

    public CustomerResponse build() {
        return new CustomerResponse(id, crmId, baseUrl, name, login);
    }

    public CustomerResponse buildSimple(){ return new CustomerResponse(id, message); }
}
