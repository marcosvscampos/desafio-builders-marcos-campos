package br.com.builders.treinamento.builders;

import br.com.builders.treinamento.domain.Customer;
import org.apache.commons.lang3.StringUtils;
import org.bson.types.ObjectId;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.function.Consumer;

public class CustomerBuilder {

        public String id;
        public String crmId;
        public String baseUrl;
        public String name;
        public String login;

        public CustomerBuilder with(
                Consumer<CustomerBuilder> builderFunction) {
            builderFunction.accept(this);
            return this;
        }

        public Customer build() {
            try {
                URL baseUrl = new URL(this.baseUrl);
                ObjectId _id;
                if(StringUtils.isEmpty(this.id)){
                     _id = ObjectId.get();
                } else {
                    _id = new ObjectId(this.id);
                }
                return new Customer(_id, crmId, baseUrl, name, login);
            } catch(MalformedURLException e){
                e.printStackTrace();
            }
            return null;
        }
    }

