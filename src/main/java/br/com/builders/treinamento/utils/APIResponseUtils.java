package br.com.builders.treinamento.utils;

import br.com.builders.treinamento.builders.CustomerResponseBuilder;
import br.com.builders.treinamento.dto.response.CustomerResponse;
import br.com.builders.treinamento.exception.ErrorMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;

import java.util.ArrayList;
import java.util.List;

public class APIResponseUtils {

    public static ResponseEntity generateSuccessStatus(Object object){
        return ResponseEntity.status(200).body(object);
    }

    public static ResponseEntity generateStatusResponse(String id, String message, Integer status){
        CustomerResponse cr = new CustomerResponseBuilder().with($ -> {
            $.id = id;
            $.message = message;
        }).buildSimple();
        return ResponseEntity.status(status).body(cr);
    }

    public static ResponseEntity generateBadRequestStatus(List<ErrorMessage> errors){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
    }

    public static List<ErrorMessage> formatErrorsList(Errors errors){
        List<ErrorMessage> errorMessages = new ArrayList<>();
        errors.getFieldErrors().forEach(er -> {
            ErrorMessage em = new ErrorMessage(er.getField(), er.getDefaultMessage(), er.getObjectName());
            errorMessages.add(em);
        });
        return errorMessages;
    }
}
